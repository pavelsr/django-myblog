from django.db import models
from django.contrib.auth.models import User
# Permission
# from django.contrib.contenttypes.models import ContentType

# import base64

# Create your models here.

# class Author(models.Model):
#     question = models.ForeignKey(Question, on_delete=models.CASCADE)
#     choice_text = models.CharField(max_length=200)
#     votes = models.IntegerField(default=0)

class Tag(models.Model):
    name = models.CharField(max_length=30)
    slug = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class Article(models.Model):
    header = models.CharField(max_length=255)
    banner = models.TextField('banner image',blank=True, null=True)
    date_creation = models.DateTimeField('creation date', auto_now_add=True, blank=True, null=True)
    date_last_edit = models.DateTimeField('last edit date', auto_now_add=True, blank=True, null=True)
    richtext = models.TextField()
    author = models.ForeignKey(User, on_delete=models.PROTECT,default=None)
    tag = models.ManyToManyField(Tag)

    # class Meta:
    #     permissions = (
    #         ( 'can_edit_own', 'Can Edit Own Articles' ),
    #     )

    # content_type = ContentType.objects.get_for_model(Article)
    # permission = Permission.objects.create(
    #     codename='can_edit_own',
    #     name='Can Edit Own Articles',
    #     content_type=content_type,
    # )

    def __str__(self):
        return self.header
