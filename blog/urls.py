from django.urls import path, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets

from . import views

router = routers.DefaultRouter()
router.register(r'users', views.UserListViewSet)
# router.register(r'users', views.UserListViewSet, basename="u")
router.register(r'tags', views.TagViewSet)
# router.register(r'articles', views.ArticleViewSet)
# router.register(r'articles', views.ArticleList.as_view(), basename="articles" )
# router.register(r'articles', views.ArticleListView, basename="articles" )


urlpatterns = [
    path('', include(router.urls)),
    path('articles/', views.ArticleListView.as_view(), name="articles"),

    # path('api', include('rest_framework.urls', namespace='rest_framework'))
]
