from django.contrib.auth.models import User, Group

from rest_framework import viewsets, generics,  filters
from django_filters.rest_framework import DjangoFilterBackend

from .serializers import UserSerializer, ArticleSerializer, TagSerializer # GroupSerializer
from .models import Article,Tag

class UserListViewSet(viewsets.ReadOnlyModelViewSet):
    """
    readonly django.contrib.auth.models.User
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


# viewsets.ModelViewSet
# viewsets.ViewSet
# generics.ListAPIView

### OK

# class ArticleListView(generics.ListAPIView):
#
#     serializer_class = ArticleSerializer
#
#     def get_queryset(self):
#         queryset = Article.objects.all()
#
#         author_username = self.request.query_params.get('author')
#         tag = self.request.query_params.get('tag')
#
#         if author_username is not None:
#             queryset = queryset.filter(author=author_username)
#         if tag is not None:
#             queryset = queryset.filter(tag=tag)
#
#         return queryset

### end of OK


# class ArticleViewSet(generics.ListAPIView):
#     serializer_class = ArticleSerializer
#
#     def get_queryset(self):
#         """
#         Optionally restricts the returned purchases to a given user,
#         by filtering against a `username` query parameter in the URL.
#         """
#         queryset = Article.objects.all()
#         username = self.request.query_params.get('username')
#         print >>sys.stderr, username


        # if username is not None:
        #     queryset = queryset.filter(article__username=username)
        # return queryset

#
# class ArticleViewSet(viewsets.ModelViewSet):
#     queryset = Article.objects.all()
#     serializer_class = ArticleSerializer
#     filter_backends = [ filters.SearchFilter ]
#     search_fields = [ 'header', 'richtext', 'author', 'tag' ]


class ArticleListView(generics.ListAPIView):

    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    search_fields = [ 'header', 'richtext' ]
    filter_fields = ['tag', 'author']

# class ArticleViewSet(viewsets.ModelViewSet):
#
#     serializer_class = ArticleSerializer
#     queryset = Article.objects.all()
#
#     def get_queryset(self):
#         """
#         filter by tag, author and search
#         """
#
#         username = self.request.query_params.get('username')
#         tag = self.request.query_params.get('tag')
#
#         if username is not None:
#             return Article.objects.filter(author=username)
#
#         if tag is not None:
#             return Article.objects.filter(tag=tag)




# class ArticleViewSet(viewsets.ViewSet):
#
#     queryset = Article.objects.all()
#     serializer_class = ArticleSerializer
#
#     filter_backends = [filters.SearchFilter]
#     search_fields = [ 'header', 'richtext', 'author', 'tag' ]
#
#     def get_queryset(self):
#         """
#         filter by tag, author and search
#         """
#         username = self.request.query_params.get('username')
#         tag = self.request.query_params.get('tag')
#
#         if username is not None:
#             queryset = queryset.filter(author=username)
#
#         if tag is not None:
#             queryset = queryset.filter(tag=tag)
#
#         return queryset


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    http_method_names = ['get', 'post', 'head']  # disable put and patch

    # def create(self, request):
    #     return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    # permission_classes = [permissions.IsAuthenticated]


# def index(request):
#     return render(request, 'blog/index.html', { 'all_articles': Article.objects.all() } )
    # return HttpResponse("Hello, world. You're at the blog index.")
