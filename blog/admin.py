from django.contrib import admin
from django.db import models

# Register your models here.

from .models import Article,Tag

import sys

# class ListArticle(admin.ModelAdmin):
#     def save_model(self, request, obj, form, change):
#         print >>sys.stderr, 'Goodbye, cruel world!'
#         obj.author = request.user
#         # banner = richtext.encode('base64')
#         # banner = base64.b64encode(richtext.encode('ascii'))
#         # banner = base64.b64encode(bytes(richtext, 'utf-8' ))
#         obj.save()

# https://docs.djangoproject.com/en/4.0/ref/contrib/admin/#django.contrib.admin.ModelAdmin

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):

    # fields = ( 'headshot_image', )
    exclude = ( 'author', )

    # formfield_overrides = {
    #     models.TextField: {'widget': RichTextEditorWidget},
    # }

    # def image1(self, obj):
    #     return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
    #         url = obj.headshot.url,
    #         width=obj.headshot.width,
    #         height=obj.headshot.height,
    #         )
    # )

    def headshot_image(self, obj):
        return mark_safe('<img src="#" />')

    # fields = ( 'banner', )
    #
    # def banner(self, obj):
    #     return format_html('<a href="#">some</a>')

    def get_queryset(self, request):

        print('get queryset', file=sys.stderr)
        # banner = self.models.ImageField();
        user_group = request.user.groups
        if user_group.filter(name='common').exists():
            return Article.objects.filter(author=request.user)
        else:
            return Article.objects.all()

    # def get(self, request):
    #     print('get one', file=sys.stderr)
    #     def banner(self, obj):
    #         return format_html('<a href="#">some</a>')



    # def change_view(self, request, object_id, form_url='', extra_context=None):
    #     object_id.author = request.user
    #     super().save_model(request, object_id, form, change)
        # banner = richtext.encode('base64')
        # banner = base64.b64encode(richtext.encode('ascii'))
        # banner = base64.b64encode(bytes(richtext, 'utf-8' ))
        # obj.save()
#
# admin.site.register(ArticleAdmin)

# admin.site.register(Article, ArticleAdmin)

admin.site.register(Tag)
