rm -rf blog/migrations
rm db.sqlite3
python3 manage.py makemigrations blog
python3 manage.py migrate
python3 manage.py createsuperuser --email admin@vk.local --username admin
# python3 manage.py loaddata sample_data.yaml

python3 manage.py loaddata groups.json
python3 manage.py loaddata users2.json

python3 manage.py loaddata tags.yaml
python3 manage.py loaddata articles.yaml

# python3 manage.py loaddata auth.json

python3 manage.py runserver

# python3 manage.py dumpdata auth --indent 4 > auth.json

# python3 manage.py dumpdata auth.User --indent 4 > users2.json
